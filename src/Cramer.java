/*
 * Author: ISC Primitivo Roman Montero
 * Instituto Tecnologico Superior de Tepeaca
 * http://trixmontero.blogspot.com
 * Modificado Sergio Martinez
 * http://es.onlinemschool.com/math/assistance/matrix/determinant/
 * http://www.resolvermatrices.com/
 **/

import java.io.*;
import java.util.Calendar;

public class Cramer {
	
	public static int[] matrizesc = {2,2,3,4,2};
	public static int[] resMatrix = {4};
	public static void main(String args[]){
		long time_start, time_end;
		int tam=0;
		
		tam = 3;
				
		int a[][]=new int[tam][tam]; //ecuacion
				
		int b[]=new int[tam]; //resultado de a
		
		int solucion[]=new int [tam];
		
		float cmr[]=new float[tam]; //temp
		
		int t=0;
		/**
		 * Matrix 
		 * 2 2 3 = 2
		 * 2 3 4 = 2
		 * 3 4 2 = 3
		 **/
		time_start = System.currentTimeMillis();
		for(int i=0; i<a.length; i++){
			for (int j=0; j<a[i].length;j++){
				a[i][j] = matrizesc[i+j]; 
				System.out.println(" matriz a = "+a[i][j]); 
			}
		}
		
		for(int i=0; i<a.length; i++){		
			b[i] =  resMatrix[0];
			System.out.println(" Res b "+(i+1)+" = "+b[i]); 
		}
		
		cmr = cramer(a, b);
		time_end = System.currentTimeMillis();
	    System.out.println("tomo "+ ( time_end - time_start ) +" millisegundos");
	}
	
	public static int determinante(int a[][]){
		int c[][]=new int[a.length+(a.length-1)][a.length];
		int det = 0;
		
		//almacena los resultados parciales
		int par[]=new int[(a.length)*2];
		
		for(int i=0;i<a.length;i++){
			for(int j=0; j<a[i].length; j++){		
			
					c[i][j]=a[i][j];		
				
			}	
		
		}
		int k=0;
		for(int i=a.length;i<c.length;i++){
			for(int j=0; j<a.length; j++){											
					c[i][j]=a[k][j];						
					
			}	
			k++;
		}
		
		//calcula la suma de los productos y la inserta en par
		k=0;
		int temp=1;
		int inc=1;
				
		for (int i=0; i< a.length;i++){
			for(int j=0; j<a[i].length;j++){
				temp=temp*c[k][j];
				k++;
			}	
			k=inc;		
			par[i]=temp;
			temp=1;
			inc++;
		}
		
		//calcula la resta de los productos y la inserta en par
		k=a.length-1;
		
		temp=1;
		inc=a.length-1;
		
		int l=(par.length)/2;
				
		for (int i=0; i< a.length;i++){
			for(int j=0; j<a[i].length;j++){
				temp=temp*c[k][j];				
				k--; //k=k-1;
			}					
			par[l]=-temp;
			temp=1;
			
			inc++;
			k=inc;
			l++;
		}
		
		det=suma(par);
		
		return det;		
	}	
	
	
	//sustituye los valores de b en a en la posicion pos
	public static int [][] sustituye(int a[][], int b[], int pos){
		int c[][] =new int[a.length][a.length];
		
		
		for(int i=0;i<a.length;i++){
			for(int j=0; j<a[i].length; j++){
				if(j==pos){
					c[i][j]=b[i];
				}
				else{
					c[i][j]=a[i][j];	
				}
				
			}	
		
		}
			
		return c;		
	}
	
	public static int suma(int a[]){
		int result=0;
		for(int i=0; i<a.length; i++){
			result=result+a[i];
		}
		
		return result;
					
	}
	
	///funcion cramer
	public static float[] cramer(int a[][], int b[]){
		float Rcramer[]=new float[b.length];
		int det=determinante(a);
		System.out.println(" determinante = "+det); 
		if(det==0){
			//JOptionPane.showMessageDialog(null,"No tiene solucion");			
			return Rcramer;
		}
		
		int detTemp;
		
		int c[][]=new int[a.length][a.length];
		for(int i=0; i<a.length; i++){
			c=sustituye(a,b,i);
			
			detTemp=determinante(c);
		
			
			Rcramer[i]=(float)detTemp/(float)det;			
		}
		return Rcramer;
	}
}