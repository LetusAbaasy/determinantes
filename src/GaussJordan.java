import java.util.*; 
public class GaussJordan 
{ 
	
	static void muestramatriz(float matriz[][], int var) 
	{ 
		for(int x=0;x<var;x++) 
		{ 
		for(int y=0;y<(var+1);y++) 
			System.out.print(" "+matriz[x][y]+" |"); 
			System.out.println(""); 
		} 
	} 
	
	static void pivote(float matriz[][],int piv,int var) 
	{ 
		float temp=0; 
		temp=matriz[piv][piv]; 
		for(int y=0;y<(var+1);y++) 
		{ 
			matriz[piv][y]=matriz[piv][y]/temp; 
		} 
	} 
	
	static void hacerceros(float matriz[][],int piv,int var) 
	{ 
		for(int x=0;x<var;x++) 
		{ 
			if(x!=piv) 
			{ 
				float c=matriz[x][piv]; 
				for(int z=0;z<(var+1);z++) 
					matriz[x][z]=((-1*c)*matriz[piv][z])+matriz[x][z]; 
			} 
		} 
	} 

	public static void main(String args[]) 
	{ 
		Scanner leer=new Scanner(System.in); 
		
		int[] matrizesc = {2,2,3,4,2,4,8};
		
		int var=0, piv=0; 
		int matriz[][]; 
		var=3; 
		matriz=new int[var][var+1]; 
		for(int x=0;x<var;x++) 
		{ 
			for(int y=0;y<(var+1);y++) 
			{ 
				//System.out.println("posicion: ["+(x+1)+"]["+(y+1)+"] = "+matrizesc[y]+" ... "); 
				matriz[x][y]= matrizesc[y+x];
				System.out.println(" a = "+matriz[x][y]); 
			}
		} 

		for(int a=0;a<var;a++) 
			{ 
			//pivote(matriz,piv,var); 
			
			//System.out.println("\tRenglon "+(a+1)+" entre el pivote"); 
			//System.out.println("matriz"+matriz[a]); 
			//muestramatriz(matriz,var); 
			System.out.println(" a = "+Arrays.toString(matriz)); 
			determinante(matriz);
			
			
			
			//System.out.println("\tHaciendo ceros"); 
			//hacerceros(matriz,piv,var); 
			
			//muestramatriz(matriz,var); 
			//System.out.println(""); 
			piv++; 
			} 
			for(int x=0;x<var;x++) 
			System.out.println("La variable X"+(x+1)+" es: "+matriz[x][var]); 
			
	} 
	
	public static int determinante(int a[][]){
		System.out.println(" a = "+Arrays.toString(a)); 
		int c[][]=new int[a.length+(a.length-1)][a.length];
		int det = 0;
		
		//almacena los resultados parciales
		int par[]=new int[(a.length)*2];
		
		for(int i=0;i<a.length;i++){
			for(int j=0; j<a[i].length; j++){		
			
					c[i][j]=a[i][j];		
				
			}	
		
		}
		int k=0;
		for(int i=a.length;i<c.length;i++){
			for(int j=0; j<a.length; j++){											
					c[i][j]=a[k][j];						
					
			}	
			k++;
		}
		
		//calcula la suma de los productos y la inserta en par
		k=0;
		int temp=1;
		int inc=1;
				
		for (int i=0; i< a.length;i++){
			for(int j=0; j<a[i].length;j++){
				temp=temp*c[k][j];
				k++;
			}	
			k=inc;		
			par[i]=temp;
			temp=1;
			inc++;
		}
		
		//calcula la resta de los productos y la inserta en par
		k=a.length-1;
		
		temp=1;
		inc=a.length-1;
		
		int l=(par.length)/2;
				
		for (int i=0; i< a.length;i++){
			for(int j=0; j<a[i].length;j++){
				temp=temp*c[k][j];				
				k--; //k=k-1;
			}					
			par[l]=-temp;
			temp=1;
			
			inc++;
			k=inc;
			l++;
		}
		
		det=suma(par);
		System.out.println(" determinante = "+det); 
		return det;		
	}
	
	public static int suma(int a[]){
		int result=0;
		for(int i=0; i<a.length; i++){
			result=result+a[i];
		}
		
		return result;
					
	}
}